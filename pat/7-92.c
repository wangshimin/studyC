/*
 * @Description  : 特殊a串数列求和
 * @version      : 
 * @Date         : 2024-03-20 15:32:13
 * @LastEditTime : 2024-03-20 15:41:27
 */
// 给定两个均不超过9的正整数a和n，要求编写程序求a+aa+aaa++⋯+aa⋯a（n个a）之和。

// 输入格式：
// 输入在一行中给出不超过9的正整数a和n。

// 输出格式：
// 在一行中按照“s = 对应的和”的格式输出。

// 输入样例：
// 2 3
// 输出样例：
// s = 246

#include <stdio.h>

int main()
{
    int a, n;
    scanf("%d %d", &a, &n);

    // 和
    int sum =0;
    int t = 0;
    // 0*10+2 2*10+2 (2*10+2)*10+2
    for (int i = 0; i < n; i++)
    {
        t = t*10+a;
        sum += t;
    }
    printf("s = %d\n", sum);
    return 0;
}